package com.bruip.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class Hello {
    @GetMapping("/jj")
    public String hello(){
        return "dddd";
    }
    @PostMapping("/kk")
    public String test(){
        return "世上";
    }
    @PostMapping("/ll")
    public Wife jxlg(){
        Wife wife = new Wife(1,"Lisa",50000);
        return wife;
    }
    @GetMapping("/mm")
    public List<Wife> wives(){
        Wife wife = new Wife(1,"Amy",20000);
        Wife wife1 = new Wife(2,"Amily",30000);
        List<Wife> list = new ArrayList<>();
        list.add(wife);
        list.add(wife1);
        return list;
    }
    @GetMapping("/person")
    public Person person(){
        Person person = new Person(1,"Mike",new Wife(3,"keyi",50000));
        return person;
    }
    @GetMapping("/teacher")
    public Teacher teacher(){
        Teacher teacher = new Teacher();
        teacher.setId(1);
        teacher.setName("keay");
        List<Wife> list = new ArrayList<>();
        list.add(new Wife(1,"peatt",500000));
        list.add(new Wife(2,"heeloo",600000));
        teacher.setWives(list);
        return teacher;
    }
    @GetMapping("/killer")
    public List<Killer> killers(){
        Killer killer = new Killer();
        Killer killer1 = new Killer();
        killer.setId(1);
        killer.setName("peate");
        List<Wife> wives = new ArrayList<>();
        wives.add(new Wife(1,"peatt",500000));
        wives.add(new Wife(2,"heeloo",600000));
        killer.setWives(wives);
        killer1.setId(2);
        killer1.setName("kiaa");
        killer1.setWives(wives);
        List<Killer> killers = new ArrayList<>();
        killers.add(killer);
        killers.add(killer1);
        return killers;
    }
    @GetMapping("/revice")
    public String revice(String name,int age){
        System.out.println(name +"-->" + age);
        return "数据已收到!";
    }
    @GetMapping("/data")
    public String data(Student student){
        System.out.println(student);
        return "数据已收到，OK";
    }
    @PostMapping("/asd")
    public String asd(@RequestBody Student student){
        System.out.println(student);
        return "数据已收到";
    }
}
