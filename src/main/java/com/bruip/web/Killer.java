package com.bruip.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Killer implements Serializable {
    private int id;
    private String name;
    private List<Wife> wives;
}
