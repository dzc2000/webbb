package com.bruip.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Wife implements Serializable {
    private int id;
    private String name;
    private double salary;
}
