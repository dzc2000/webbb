package com.bruip.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WebbbHelloApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebbbHelloApplication.class, args);
    }

}
